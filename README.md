# C2Crystal - Generate Native Bindings for Crystal
Quick python script.

Just ```python runc2cr.py```.
Generated files will be in the [bindings](bindings/) folder.  

C2Crystal will link to the library using however you may need to use ```--link-flags``` to link any other libraries, frameworks, library folders, etc.  
***C2Crystal only generates bindings for header files***.